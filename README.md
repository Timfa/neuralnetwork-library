A simple Neural Network library.
Created by Tim Falken.

The library allows for easy creations of Neural Networks with an arbitrary amount of layers.
Supports a few built-in activation methods for neurons, as well as defining custom ones.

Included in the project is a simple Tester project, which makes a network and teaches it the XOR function.
Note that this test-network is not at all optimized, and is only included to show how a network could be built.

<h1>Downloads:</h1>

<a href="https://gitlab.com/Timfa/neuralnetwork-library/raw/master/NeuralNetwork/bin/Debug/NeuralNetwork.dll">NeuralNetwork Base DLL</a> 

<a href="https://gitlab.com/Timfa/neuralnetwork-library/raw/master/NeuralNetworkTrainer/bin/Debug/NeuralNetworkTrainer.dll">Optional NeuralNetwork Trainer Utility DLL</a>