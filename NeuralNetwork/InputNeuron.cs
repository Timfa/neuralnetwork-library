﻿namespace MachineLearning
{
    /// <summary>
    /// A special neuron for use in an input layer. Does not have connections going into the network, but instead provides output based on manually-assigned input values.
    /// </summary>
    public class InputNeuron : NeuralNetworkNeuron
    {
        /// <summary>
        /// The input value this neuron will provide to the network.
        /// </summary>
        public double Input = 0;

        /// <summary>
        /// Get the input neuron value.
        /// </summary>
        /// <returns>The input value of this neuron.</returns>
        public override double GetValue()
        {
            return Input;
        }
    }
}
