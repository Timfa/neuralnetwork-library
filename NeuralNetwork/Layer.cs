﻿using System;
using System.Collections.Generic;

namespace MachineLearning
{
    /// <summary>
    /// A layer consisting of a number of neurons.
    /// </summary>
    public class NeuralNetworkLayer
    {
        private static Random rand = new Random();
        public NeuralNetworkNeuron.ActivationTypes ActivationType { get; private set; }

        /// <summary>
        /// The neurons in the layer.
        /// </summary>
        public List<NeuralNetworkNeuron> Neurons = new List<NeuralNetworkNeuron>();

        private NeuralNetworkLayer()
        {

        }

        /// <summary>
        /// Add a neuron to the layer.
        /// </summary>
        /// <param name="neuron">The neuron to add.</param>
        public void AddNeuron(NeuralNetworkNeuron neuron)
        {
            Neurons.Add(neuron);
        }

        /// <summary>
        /// Automatically connect two layers' neurons with Connections
        /// </summary>
        /// <param name="from">The layer that will provide output for the next layer.</param>
        /// <param name="to">The layer that will accept input from the previous layer.</param>
        /// <param name="randomWeights">Automatically randomize connection weights.</param>
        public static void ConnectLayers(NeuralNetworkLayer from, NeuralNetworkLayer to, double randomization = 0)
        {
            foreach(NeuralNetworkNeuron fromNeuron in from.Neurons)
            {
                if(fromNeuron.ConnectionsOut.Count != 0)
                    throw new InvalidOperationException("Neuron in FROM layer already connected");
            }

            foreach(NeuralNetworkNeuron toNeuron in to.Neurons)
            {
                if(toNeuron.ConnectionsIn.Count != 0)
                    throw new InvalidOperationException("Neuron in TO layer already connected");
            }

            foreach(NeuralNetworkNeuron fromNeuron in from.Neurons)
            {
                foreach(NeuralNetworkNeuron toNeuron in to.Neurons)
                {
                    Connection connection;
                    connection = new Connection(fromNeuron, toNeuron, 1 + (rand.NextDouble() * randomization));
                }
            }
        }

        /// <summary>
        /// Create an input layer, consisting of input neurons.
        /// </summary>
        /// <param name="amount">The amount of neurons in the input layer.</param>
        /// <returns>The new input layer.</returns>
        public static NeuralNetworkLayer InputLayer(int amount)
        {
            NeuralNetworkLayer result = new NeuralNetworkLayer();

            for (int i = 0; i < amount; i++)
            {
                result.AddNeuron(new InputNeuron());
            }

            return result;
        }

        /// <summary>
        /// Create a normal layer of neurons.
        /// </summary>
        /// <param name="amount">The amount of neurons in the layer.</param>
        /// <param name="activationType">The activation type of the neuron.</param>
        /// <returns>The new layer.</returns>
        public static NeuralNetworkLayer NormalLayer(int amount, NeuralNetworkNeuron.ActivationTypes activationType = NeuralNetworkNeuron.ActivationTypes.Linear)
        {
            NeuralNetworkLayer result = new NeuralNetworkLayer();

            result.ActivationType = activationType;

            for(int i = 0; i < amount; i++)
            {
                result.AddNeuron(new NeuralNetworkNeuron(activationType));
            }

            return result;
        }
    }
}
