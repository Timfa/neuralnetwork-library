﻿using System;
using System.Collections.Generic;

namespace MachineLearning
{
    public class State
    {
        internal string StateString;
        public StateAction[] StateActions;
        internal int TimesVisited = 0;

        internal bool HasUnvisitedPaths = true;

        public StateAction[] UnvisitedPaths()
        {
            if (!HasUnvisitedPaths)
                return new StateAction[0];

            List<StateAction> unvisited = new List<StateAction>();

            for(int i = 0; i < StateActions.Length; i++)
            {
                if(StateActions[i].Unvisited)
                {
                    unvisited.Add(StateActions[i]);
                }
            }

            if (unvisited.Count == 0)
                HasUnvisitedPaths = false;

            return unvisited.ToArray();
        }

        public State(string stateString, StateAction[] stateActions)
        {
            StateString = stateString;
            StateActions = stateActions;
        }

        public StateAction BestPath()
        {
            StateAction[] paths = UnvisitedPaths();

            if (paths.Length == 0)
            {
                StateAction best = null;
                double rating = 0;

                for(int i = 0; i < StateActions.Length; i++)
                {
                    double w = StateActions[i].GetWeight();

                    if (best != null && !best.LeadsToUnvisited && StateActions[i].LeadsToUnvisited)
                        rating = -1;

                    if (best != null && best.LeadsToUnvisited && !StateActions[i].LeadsToUnvisited)
                        w = -1;

                    if (w > rating)
                    {
                        rating = w;
                        best = StateActions[i];
                    }
                }

                return best;
            }
            else
            {
                return paths[0];
            }
        }
    }
}