﻿using System.Collections.Generic;

namespace MachineLearning
{
    public class StateAction : IWeightedItem
    {
        public int ActionEvaluation = 0;
        public string ActionString = "";
        public string ResultStateString = "";
        public bool WasEvaluated = false;
        private int evaluationTracker = -1;
        private double cachedEvaluation = 0;
        private int cachedDeepEvaluation = 0;
        private int cachedAdds = 0;
        public bool Unvisited = true;
        public bool LeadsToUnvisited = false;

        private static int deepEvalCount = 0;
        private int deepEvalTracker = -1;

        public StateAgent parentMap;

        public StateAction(string actionString, int defaultEvaluation = 100)
        {
            ActionString = actionString;
            ActionEvaluation = defaultEvaluation;
        }

        public EvaluationResult GetDeepEvaluation(EvaluationResult evaluation, int currentDepth = 0, List<string> visitedStates = null)
        {
            deepEvalTracker = deepEvalCount;

            if (Unvisited)
            {
                evaluation.containsUnvisited = true;
            }

            if (evaluationTracker == parentMap.totalEvaluations)
            {
                evaluation.evaluation = cachedDeepEvaluation;
                evaluation.totalAdds = cachedAdds;
            }
            else
            {
                if (visitedStates == null)
                    visitedStates = new List<string>();

                evaluation.evaluation += ActionEvaluation;
                evaluation.totalAdds++;

                if (ResultStateString.Length > 0 && parentMap != null)
                {
                    if (visitedStates.Contains(ResultStateString))
                    {
                        evaluation.evaluation--;
                        evaluation.evaluation -= ActionEvaluation;
                    }
                    else
                    {
                        visitedStates.Add(ResultStateString);
                        State resultState = parentMap.GetStateByString(ResultStateString);

                        if (resultState != null)
                        {
                            for (int i = 0; i < resultState.StateActions.Length; i++)
                            {
                                if (currentDepth <= parentMap.PredicionDepth && resultState.StateActions[i].deepEvalTracker != deepEvalCount)
                                {
                                    evaluation = resultState.StateActions[i].GetDeepEvaluation(evaluation, currentDepth + 1);
                                }
                            }
                        }
                    }
                }

                cachedDeepEvaluation = evaluation.evaluation;
                cachedAdds = evaluation.totalAdds;
            }

            return evaluation;
        }

        public double GetWeight()
        {
            if (evaluationTracker == parentMap.totalEvaluations)
            {
                return cachedEvaluation;
            }
            else
            {
                deepEvalCount++;
                EvaluationResult evaluation = GetDeepEvaluation(new EvaluationResult());

                int result = evaluation.totalAdds > 0 ? (evaluation.evaluation / evaluation.totalAdds) : evaluation.evaluation;

                cachedEvaluation = result;
                evaluationTracker = parentMap.totalEvaluations;

                LeadsToUnvisited = evaluation.containsUnvisited;

                return cachedEvaluation;
            }
        }

        public struct EvaluationResult
        {
            public int evaluation, totalAdds;
            public bool containsUnvisited;
        }
    }
}