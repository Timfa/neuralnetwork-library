﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MachineLearning
{
    public static class WeightedRandomHelper
    {
        private static Random random = new Random();

        public static T RandomWeighted<T>(this IEnumerable<T> items) where T : IWeightedItem
        {
            if (!items.Any())
                return default(T);

            IOrderedEnumerable<T> source = items.OrderBy(i => i.GetWeight());

            double dTotalWeight = source.Sum(i => i.GetWeight());

            double dRandom = random.NextDouble() * dTotalWeight;
            double cumulative = 0;

            for(int i = 0; i < source.Count(); i++)
            {
                T element = source.ElementAt(i);
                double weight = element.GetWeight();
                cumulative += weight;

                if (dRandom <= cumulative)
                    return element;
            }

            throw new Exception("Could not select item");
              /*
            while (true)
            {
                double dRandom = random.NextDouble() * dTotalWeight;

                if (dTotalWeight == 0)
                    dRandom = -1;

                foreach (T item in source)
                {
                    if (dRandom < item.GetWeight())
                        return item;

                    dRandom -= item.GetWeight();
                }
            }*/
        }
    }
}
