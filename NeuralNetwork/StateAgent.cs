﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MachineLearning
{
    public class StateAgent
    {
        private int stateIndex = 0;

        private List<State> states;

        private StateAction lastAction = null;

        public int PredicionDepth = 10;
        public int totalEvaluations = 0;

        public State CurrentState
        {
            get
            {
                return states[stateIndex];
            }
        }

        public int EvaluatedActions
        {
            get
            {
                int count = 0;

                foreach (State state in states)
                {
                    foreach (StateAction action in state.StateActions)
                    {
                        if (action.WasEvaluated)
                            count++;
                    }
                }

                return count;
            }
        }

        public int LearnedStates
        {
            get
            {
                return states.Count;
            }
        }

        public StateAgent(State initialState, int predictionDepth = 10)
        {
            states = new List<State>();
            states.Add(initialState);

            foreach (StateAction action in states[stateIndex].StateActions)
            {
                action.parentMap = this;
            }

            PredicionDepth = predictionDepth;
        }

        public StateAction[] GetAllStateActions()
        {
            return states[stateIndex].StateActions;
        }

        public StateAction GetChosenActionForCurrentState()
        {
            return states[stateIndex].BestPath();
        }

        public State GetStateByString(string stateString)
        {
            return states.Find(o => o.StateString.Equals(stateString));
        }

        public void EvaluateAdditive(int score)
        {
            if(lastAction != null)
            {
                int pEval = lastAction.ActionEvaluation;

                lastAction.ActionEvaluation += score;
                lastAction.WasEvaluated = true;

                if (lastAction.ActionEvaluation < 1)
                    lastAction.ActionEvaluation = 1;

                if(pEval != lastAction.ActionEvaluation)
                    totalEvaluations++;
            }
        }

        public void EvaluateAbsolute(int score)
        {
            if (lastAction != null)
            {
                int pEval = lastAction.ActionEvaluation;

                lastAction.ActionEvaluation = score;
                lastAction.WasEvaluated = true;

                if (lastAction.ActionEvaluation < 1)
                    lastAction.ActionEvaluation = 1;

                if (pEval != lastAction.ActionEvaluation)
                    totalEvaluations++;
            }
        }

        public void SetState(State state)
        {
            State foundState = GetStateByString(state.StateString);

            if (foundState == null)
            {
                states.Add(state);
                stateIndex = states.Count - 1;
            }
            else
            {
                stateIndex = states.IndexOf(foundState);
                foundState.TimesVisited++;
            }

            foreach (StateAction action in states[stateIndex].StateActions)
            {
                action.parentMap = this;
            }
        }

        public void PerformStateAction(StateAction action, State resultState)
        {
            action.ResultStateString = resultState.StateString;
            lastAction = action;
            action.Unvisited = false;
            SetState(resultState);
        }
    }
}
