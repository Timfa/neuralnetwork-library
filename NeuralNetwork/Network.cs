﻿using System;
using System.Collections.Generic;

namespace MachineLearning
{
    /// <summary>
    /// A Neural Network consisting of multiple layers of neurons.
    /// </summary>
    public class NeuralNetwork : IWeightedItem
    {
        /// <summary>
        /// An unused value which you can use for storing the network's fitness. (Optional)
        /// </summary>
        public double Fitness = 0;

        private static Random random = new Random();
        public List<NeuralNetworkLayer> Layers = new List<NeuralNetworkLayer>();

        /// <summary>
        /// A Neural Network consisting of multiple layers of neurons.
        /// </summary>
        /// <param name="inputLayerNeurons">Amount of neurons in the input layer.</param>
        public NeuralNetwork(int inputLayerNeurons, double randomization = 0)
        {
            Layers.Add(NeuralNetworkLayer.InputLayer(inputLayerNeurons));
        }

        /// <summary>
        /// Add a new layer to the network and automatically connect it to the previous layer.
        /// </summary>
        /// <param name="amountOfNeurons">Amount of neurons in the new layer.</param>
        /// <param name="activationType">The activation type of the neurons in the layer.</param>
        /// <param name="randomWeights">Automatically randomize connection weights.</param>
        /// <returns></returns>
        public NeuralNetworkLayer AddLayer(int amountOfNeurons, NeuralNetworkNeuron.ActivationTypes activationType = NeuralNetworkNeuron.ActivationTypes.Sigmoid, double randomization = 0)
        {
            NeuralNetworkLayer newLayer = NeuralNetworkLayer.NormalLayer(amountOfNeurons, activationType);

            NeuralNetworkLayer.ConnectLayers(Layers[Layers.Count - 1], newLayer, randomization);

            Layers.Add(newLayer);

            return newLayer;
        }

        /// <summary>
        /// Get the output neuron values based on a set of input values.
        /// </summary>
        /// <param name="inputs">The inputs to provide to the input neurons.</param>
        /// <returns>An array of doubles that correspond to the output neurons.</returns>
        public double[] GetOutput(double[] inputs)
        {
            if(inputs.Length != Layers[0].Neurons.Count)
                throw new InvalidOperationException("Amount of inputs don't match amount of input neurons");

            double[] outputs = new double[Layers[Layers.Count - 1].Neurons.Count];

            NeuralNetworkLayer inputLayer = Layers[0];
            for(int i = 0; i < inputs.Length; i++)
            {
                ((InputNeuron)inputLayer.Neurons[i]).Input = inputs[i];
            }

            NeuralNetworkLayer outputLayer = Layers[Layers.Count - 1];
            for(int i = 0; i < outputLayer.Neurons.Count; i++)
            {
                outputs[i] = outputLayer.Neurons[i].GetValue();
            }

            return outputs;
        }

        /// <summary>
        /// Randomly mutate the network's parameters. Also resets the Fitness to zero.
        /// </summary>
        /// <param name="strength">The amount of difference in neuron/connection bias. The higher the number, the stronger the mutation.</param>
        /// <param name="method">The method in which to mutate the network.</param>
        public void Mutate(double strength = 1)
        {
            Fitness = 0;
            
            for(int i = 1; i < Layers.Count; i++)
            {
                NeuralNetworkLayer layer = Layers[i];

                for(int l = 0; l < layer.Neurons.Count; l++)
                {
                    layer.Neurons[l].Bias += RandomRange(-strength, strength);

                    for(int conn = 0; conn < layer.Neurons[l].ConnectionsIn.Count; conn++)
                    {
                        layer.Neurons[l].ConnectionsIn[conn].Weight += RandomRange(-strength, strength);
                    }
                }
            }
        }

        private double RandomRange(double min, double max)
        {
            double d = max - min;

            return min + (random.NextDouble() * d);
        }

        /// <summary>
        /// Create a clone of the current network.
        /// </summary>
        /// <returns>A clone of this network.</returns>
        public NeuralNetwork Clone()
        {
            NeuralNetwork newNetwork = new NeuralNetwork(Layers[0].Neurons.Count);

            newNetwork.Fitness = Fitness;

            for (int i = 1; i < Layers.Count; i++)
            {
                NeuralNetworkLayer layer = newNetwork.AddLayer(Layers[i].Neurons.Count);

                for (int l = 0; l < layer.Neurons.Count; l++)
                {
                    layer.Neurons[l].Bias = Layers[i].Neurons[l].Bias;

                    layer.Neurons[l].ActivationType = Layers[i].Neurons[l].ActivationType.Clone();

                    for (int c = 0; c < layer.Neurons[l].ConnectionsIn.Count; c++)
                    {
                        layer.Neurons[l].ConnectionsIn[c].Weight = Layers[i].Neurons[l].ConnectionsIn[c].Weight;
                    }
                }
            }

            return newNetwork;
        }

        /// <summary>
        /// Types of mutation methods.
        /// </summary>
        private enum MutationMethods
        {
            /// <summary>
            /// Only change the weights of connections, and neuron bias.
            /// </summary>
            WeightsOnly,

            /// <summary>
            /// Change the weights and neuron bias, and add/remove neurons. (Note: Only adds neurons with activation types already present in the layer)
            /// </summary>
            WeightsAndNeuronAmounts,

            /// <summary>
            /// Change the weights and neuron bias, and add/remove neurons and layers. (Note: Only adds neurons with activation types already present in the network)
            /// </summary>
            WeightsAndLayers
        }

        /// <summary>
        /// Convert the network to a string, which could be saved to a file.
        /// </summary>
        /// <returns>A string representing the Network.</returns>
        public override string ToString()
        {
            string str = Fitness.ToString();

            foreach(NeuralNetworkLayer layer in Layers)
            {
                str += "L" + (int)layer.ActivationType + "l";

                foreach(NeuralNetworkNeuron neuron in layer.Neurons)
                {
                    str += "N" + neuron.Bias + "n";

                    foreach(Connection connection in neuron.ConnectionsOut)
                    {
                        str += "C" + connection.Weight;
                    }
                }
            }

            return str;
        }

        /// <summary>
        /// Parse a Network string to a network.
        /// </summary>
        /// <param name="networkString">The Network string to parse.</param>
        /// <returns>A new Network instance.</returns>
        public static NeuralNetwork Parse(string networkString)
        {
            try
            {
                string[] layerStrings = networkString.Split("L".ToCharArray()[0]);

                LayerData[] layerData = new LayerData[layerStrings.Length - 1];

                for(int i = 0; i < layerStrings.Length - 1; i++)
                {
                    string layerStr = layerStrings[i + 1];

                    string[] split = layerStr.Split("l".ToCharArray()[0]);

                    int layerType = int.Parse(split[0]);

                    layerData[i] = new LayerData();

                    layerData[i].ActivationType = (NeuralNetworkNeuron.ActivationTypes)layerType;

                    string[] neuronStrings = split[1].Split("N".ToCharArray()[0]);

                    layerData[i].Neurons = new NeuronData[neuronStrings.Length - 1];

                    for(int n = 0; n < neuronStrings.Length - 1; n++)
                    {
                        string neuronStr = neuronStrings[n + 1];

                        string[] nsplit = neuronStr.Split("n".ToCharArray()[0]);

                        double bias = double.Parse(nsplit[0]);

                        layerData[i].Neurons[n] = new NeuronData();

                        layerData[i].Neurons[n].Bias = bias;

                        string[] connectionStrings = nsplit[1].Split("C".ToCharArray()[0]);

                        layerData[i].Neurons[n].Connections = new ConnectionData[connectionStrings.Length - 1];

                        for(int c = 0; c < connectionStrings.Length - 1; c++)
                        {
                            string connectionStr = connectionStrings[c + 1];

                            double weight = double.Parse(connectionStr);

                            layerData[i].Neurons[n].Connections[c] = new ConnectionData();

                            layerData[i].Neurons[n].Connections[c].Weight = weight;
                        }
                    }
                }

                NeuralNetwork network = new NeuralNetwork(layerData[0].Neurons.Length);

                network.Fitness = double.Parse(layerStrings[0]);

                for(int i = 1; i < layerData.Length; i++)
                {
                    network.AddLayer(layerData[i].Neurons.Length, layerData[i].ActivationType);
                }

                List<NeuralNetworkLayer> Layers = network.Layers;

                for(int i = 0; i < Layers.Count; i++)
                {
                    NeuralNetworkLayer layer = Layers[i];

                    for(int l = 0; l < layer.Neurons.Count; l++)
                    {
                        layer.Neurons[l].Bias = layerData[i].Neurons[l].Bias;

                        for(int c = 0; c < layer.Neurons[l].ConnectionsOut.Count; c++)
                        {
                            layer.Neurons[l].ConnectionsOut[c].Weight = layerData[i].Neurons[l].Connections[c].Weight;
                        }
                    }
                }

                return network;
            }
            catch(Exception e)
            {
                throw new FormatException("The provided string is not a valid Network string.", e);
            }
        }

        public double GetWeight()
        {
            return Fitness;
        }

        private class LayerData
        {
            public NeuralNetworkNeuron.ActivationTypes ActivationType;

            public NeuronData[] Neurons;
        }

        private class NeuronData
        {
            public double Bias;

            public ConnectionData[] Connections;
        }

        private class ConnectionData
        {
            public double Weight;
        }
    }
}
