﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MachineLearning
{
    public class ReinforcedAgent
    {
        private NeuralNetwork bestBrain, currentBrain;
        private int learnStepSize = 1;
        private int learnCounter = 0;
        private double learnSpeed = 1;

        public int Evolutions { get; private set; }
        public int Resets { get; private set; }

        public bool ImprovementsOnly = true; //Set to false to accept any result with Fitness > 0
        public bool LowerSuccessBarrierOnFail = true;

        public override string ToString()
        {
            string result = "";

            result += bestBrain.ToString() + "::";          //0 - network
            result += currentBrain.ToString() + "::";       //1 - network
            result += learnStepSize.ToString() + "::";      //2 - int
            result += learnCounter.ToString() + "::";       //3 - int
            result += learnSpeed.ToString() + "::";         //4 - double
            result += Evolutions.ToString() + "::";         //5 - int
            result += Resets.ToString() + "::";             //6 - int
            result += ImprovementsOnly.ToString() + "::";   //7 - bool
            result += LowerSuccessBarrierOnFail.ToString(); //8 - bool

            return result;
        }

        public static ReinforcedAgent Parse(string str)
        {
            ReinforcedAgent result = null;

            try
            {
                string[] bits = Regex.Split(str, "::");
                NeuralNetwork best = NeuralNetwork.Parse(bits[0]);
                NeuralNetwork current = NeuralNetwork.Parse(bits[1]);

                result = new ReinforcedAgent(best);

                result.bestBrain = best;
                result.currentBrain = current;
                result.learnStepSize = int.Parse(bits[2]);
                result.learnCounter = int.Parse(bits[3]);
                result.learnSpeed = double.Parse(bits[4]);
                result.Evolutions = int.Parse(bits[5]);
                result.Resets = int.Parse(bits[6]);
                result.ImprovementsOnly = bool.Parse(bits[7]);
                result.LowerSuccessBarrierOnFail = bool.Parse(bits[8]);
            }
            catch(Exception e)
            {
                throw new FormatException("Agent string was not correctly formatted", e);
            }

            return result;
        }
        
        public ReinforcedAgent(NeuralNetwork startBrain, double learnSpeed = 1, int learnStepSize = 1)
        {
            bestBrain = startBrain.Clone();
            currentBrain = startBrain.Clone();

            bestBrain.Mutate(learnSpeed);
            currentBrain.Mutate(learnSpeed);

            bestBrain.Fitness = -int.MaxValue;

            this.learnStepSize = learnStepSize;
            this.learnSpeed = learnSpeed;
        }

        public double[] GetOutput(double[] inputs)
        {
            return currentBrain.GetOutput(inputs);
        }

        public void Reward(double reward = 1)
        {
            currentBrain.Fitness += reward;
            CountLearnStep();
        }

        public void Punish(double punishment = 1)
        {
            currentBrain.Fitness -= punishment;
            CountLearnStep();
        }

        private void CountLearnStep()
        {
            learnCounter++;

            if (learnCounter >= learnStepSize)
            {
                Evaluate();
                learnCounter = 0;
            }
        }

        private void Evaluate()
        {
            if (currentBrain.Fitness > bestBrain.Fitness)
            {
                bestBrain = currentBrain.Clone();
                bestBrain.Fitness = currentBrain.Fitness < 0 || ImprovementsOnly ? currentBrain.Fitness : 0;

                Evolutions++;
            }
            else
            {
                currentBrain = bestBrain.Clone();
                currentBrain.Mutate(learnSpeed);

                if(LowerSuccessBarrierOnFail)
                {
                    bestBrain.Fitness -= Math.Abs(bestBrain.Fitness * 0.05);
                }

                Resets++;
            }
        }
    }

}
