﻿namespace MachineLearning
{
    /// <summary>
    /// A connection between two neurons.
    /// </summary>
    public class Connection
    {
        public static int totalConnections;

        public double Weight = 1;

        internal NeuralNetworkNeuron neuronFrom, neuronTo;

        /// <summary>
        /// A connection between two neurons.
        /// </summary>
        /// <param name="neuronFrom">The neuron from the previous layer to connect.</param>
        /// <param name="neuronTo">The destination neuron in the next layer to connect.</param>
        /// <param name="weight">The starting weight of the connection.</param>
        public Connection (NeuralNetworkNeuron neuronFrom, NeuralNetworkNeuron neuronTo, double weight = 1)
        {
            this.neuronFrom = neuronFrom;
            this.neuronTo = neuronTo;

            Weight = weight;

            neuronFrom.ConnectOut(this);
            neuronTo.ConnectIn(this);

            totalConnections++;
        }

        /// <summary>
        /// Get the output of this neuron.
        /// </summary>
        /// <returns>A double between 0 and 1</returns>
        public double GetWeightedNeuronOutput()
        {
            return neuronFrom.GetValue() * Weight;
        }

        ~Connection()
        {
            totalConnections--;
        }
    }
}
