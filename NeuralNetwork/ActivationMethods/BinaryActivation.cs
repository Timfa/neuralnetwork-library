﻿using System;

namespace MachineLearning.ActivationMethods
{
    /// <summary>
    /// Returns either 1 or 0. Not recommended to be used as output layer; learning will be difficult.
    /// </summary>
    public class BinaryActivation : NeuronActivationMethod
    {
        public override double ProcessInput(double input)
        {
            return Math.Round(Math.Min(1, Math.Max(0, input)));
        }

        public override NeuronActivationMethod Clone()
        {
            return new BinaryActivation();
        }
    }
}
