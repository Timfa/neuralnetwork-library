﻿using System;

namespace MachineLearning.ActivationMethods
{
    /// <summary>
    /// Returns any number from 0 to infinity.
    /// </summary>
    public class ReLUActivation : NeuronActivationMethod
    {
        public override double ProcessInput(double input)
        {
            return Math.Max(0, input);
        }

        public override NeuronActivationMethod Clone()
        {
            return new ReLUActivation();
        }
    }
}
