﻿namespace MachineLearning
{
    /// <summary>
    /// An activation method for a neuron.
    /// </summary>
    public abstract class NeuronActivationMethod
    {
        public abstract double ProcessInput(double input);
        public abstract NeuronActivationMethod Clone();
    }
}
