﻿namespace MachineLearning.ActivationMethods
{
    /// <summary>
    /// Directly outputs weighted inputs.
    /// </summary>
    public class LinearActivation : NeuronActivationMethod
    {
        public override double ProcessInput(double input)
        {
            return input;
        }

        public override NeuronActivationMethod Clone()
        {
            return new LinearActivation();
        }
    }
}
