﻿using System;

namespace MachineLearning.ActivationMethods
{
    /// <summary>
    /// Returns the sigmoid of the input.
    /// </summary>
    public class SigmoidActivation : NeuronActivationMethod
    {
        public override double ProcessInput(double input)
        {
            return Sigmoid(input);
        }

        /// <summary>
        /// Calculate the Sigmoid of the input double.
        /// </summary>
        /// <param name="input"></param>
        /// <returns>A double between 0 and 1.</returns>
        public static double Sigmoid(double input)
        {
            //return 1 / (1 + Math.Exp(-input));
            return ((1 / (1 + Math.Exp(input))) - 0.5) * -2;
        }

        public override NeuronActivationMethod Clone()
        {
            return new SigmoidActivation();
        }
    }
}
