﻿using System;

namespace MachineLearning.ActivationMethods
{
    /// <summary>
    /// A variant of ReLU which is smoother. Returns any number from 0 to infinity.
    /// </summary>
    public class SoftPlusActivation : NeuronActivationMethod
    {
        public override double ProcessInput(double input)
        {
            return Math.Log(Math.Exp(input) + 1);
        }

        public override NeuronActivationMethod Clone()
        {
            return new SoftPlusActivation();
        }
    }
}
