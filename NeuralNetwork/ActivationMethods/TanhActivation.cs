﻿using System;

namespace MachineLearning.ActivationMethods
{
    /// <summary>
    /// Returns the tangens hyperbolicus of the input.
    /// </summary>
    public class TanhActivation : NeuronActivationMethod
    {
        public override double ProcessInput(double input)
        {
            return Math.Tanh(input);
        }

        public override NeuronActivationMethod Clone()
        {
            return new TanhActivation();
        }
    }
}
