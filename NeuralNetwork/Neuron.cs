﻿using MachineLearning.ActivationMethods;
using System.Collections.Generic;

namespace MachineLearning
{
    /// <summary>
    /// A Neuron to be used in a network.
    /// </summary>
    public class NeuralNetworkNeuron
    {
        /// <summary>
        /// Connections going into this neuron.
        /// </summary>
        public List<Connection> ConnectionsIn = new List<Connection>();

        /// <summary>
        /// Connections going out from this neuron.
        /// </summary>
        public List<Connection> ConnectionsOut = new List<Connection>();

        /// <summary>
        /// The neuron's bias value.
        /// </summary>
        public double Bias = 0;

        /// <summary>
        /// The neuron's activation method.
        /// </summary>
        public NeuronActivationMethod ActivationType = null;

        /// <summary>
        /// A Neuron to be used in a network.
        /// </summary>
        /// <param name="bias">Starting bias value.</param>
        /// <param name="activationType">Activation method for the neuron.</param>
        public NeuralNetworkNeuron(ActivationTypes activationType = ActivationTypes.Linear)
        {
            switch(activationType)
            {
                case ActivationTypes.Sigmoid:
                    ActivationType = new SigmoidActivation();
                    break;

                case ActivationTypes.Tanh:
                    ActivationType = new TanhActivation();
                    break;

                case ActivationTypes.ReLU:
                    ActivationType = new ReLUActivation();
                    break;

                case ActivationTypes.SoftPlus:
                    ActivationType = new SoftPlusActivation();
                    break;

                case ActivationTypes.Binary:
                    ActivationType = new BinaryActivation();
                    break;

                case ActivationTypes.Linear:
                default:
                    ActivationType = new LinearActivation();
                    break;
            }
        }

        /// <summary>
        /// A Neuron to be used in a network.
        /// </summary>
        /// <param name="bias">Starting bias value.</param>
        /// <param name="activationMethod">Custom activation method for the neuron.</param>
        public NeuralNetworkNeuron(NeuronActivationMethod activationMethod)
        {
            ActivationType = activationMethod;
        }

        /// <summary>
        /// Add a new connection going into this neuron.
        /// </summary>
        /// <param name="connection">The connection to register</param>
        public void ConnectIn(Connection connection)
        {
            if (!ConnectionsIn.Contains(connection))
            {
                ConnectionsIn.Add(connection);
            }
        }

        /// <summary>
        /// Add a new connection going out of this neuron.
        /// </summary>
        /// <param name="connection">The connection to register.</param>
        public void ConnectOut(Connection connection)
        {
            if(!ConnectionsOut.Contains(connection))
            {
                ConnectionsOut.Add(connection);
            }
        }

        /// <summary>
        /// Remove a connection from this neuron.
        /// </summary>
        /// <param name="connection">The connection to remove.</param>
        public void Disconnect(Connection connection)
        {
            if(ConnectionsIn.Contains(connection))
            {
                ConnectionsIn.Remove(connection);
            }

            if(ConnectionsOut.Contains(connection))
            {
                ConnectionsOut.Remove(connection);
            }
        }

        /// <summary>
        /// Remove all connections from this neuron.
        /// </summary>
        public void DisconnectAll()
        {
            DisconnectAllIncoming();
            DisconnectAllOutgoing();
        }

        /// <summary>
        /// Remove all outgoing connections.
        /// </summary>
        public void DisconnectAllOutgoing()
        {
            foreach(Connection conn in ConnectionsOut)
            {
                conn.neuronTo.Disconnect(conn);
            }

            ConnectionsOut = new List<Connection>();
        }

        /// <summary>
        /// Remove all incoming connections.
        /// </summary>
        public void DisconnectAllIncoming()
        {
            foreach(Connection conn in ConnectionsIn)
            {
                conn.neuronFrom.Disconnect(conn);
            }

            ConnectionsIn = new List<Connection>();
        }

        /// <summary>
        /// Get the weighted input from all neurons connected to this neuron's input.
        /// </summary>
        /// <returns>the total input value.</returns>
        public double GetWeightedInputs()
        {
            double val = 0;

            foreach(Connection connection in ConnectionsIn)
            {
                val += connection.GetWeightedNeuronOutput();
            }

            return val + Bias;
        }

        /// <summary>
        /// Get the value this neuron outputs based on its inputs.
        /// </summary>
        /// <returns>The neuron output.</returns>
        public virtual double GetValue()
        {
            return ActivationType.ProcessInput(GetWeightedInputs());
        }
        
        /// <summary>
        /// Neuron activation types.
        /// </summary>
        public enum ActivationTypes
        {
            /// <summary>
            /// 0...1 -> 1 / (1 + Math.Exp(-input));
            /// </summary>
            Sigmoid,

            /// <summary>
            /// -1...1 -> Math.Tanh(input);
            /// </summary>
            Tanh,

            /// <summary>
            /// 0...infinity -> Math.Max(0, input);
            /// </summary>
            ReLU,

            /// <summary>
            /// 0...infinity -> Math.Log(Math.Exp(input) + 1);
            /// </summary>
            SoftPlus,

            /// <summary>
            /// 0/1 ->Math.Round( Math.Min(1, Math.Max(0, input)) ); (Same as linear, but rounded to 1 or 0)
            /// </summary>
            Binary,

            /// <summary>
            /// -infinity...infinity -> directly outputs weighted input;
            /// </summary>
            Linear
        }
    }
}
