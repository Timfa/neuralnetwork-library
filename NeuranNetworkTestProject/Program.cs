﻿using MachineLearning;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningTester
{
    class Program
    {
        static void Main(string[] args)
        {
            ReinforcedAgent agent = new ReinforcedAgent(new NeuralNetwork(2));
            string str = agent.ToString();

            agent = ReinforcedAgent.Parse(str);

            Console.WriteLine("Press RETURN to start!");
            Console.Read();

            // Set up a simple network of an arbitrary amount of layers.
            NeuralNetwork network = new NeuralNetwork(2);
            network.AddLayer(2, NeuralNetworkNeuron.ActivationTypes.Sigmoid);
            network.AddLayer(1, NeuralNetworkNeuron.ActivationTypes.Sigmoid);
            
            //Teach the network the XOR function
            XOR(network);
            
            Console.Read();
            Console.Read();
        }

        static void XOR(NeuralNetwork network)
        {
            //Create a new trainer. Trainer is for convenience; You could also train networks manually.
            NetworkTrainer trainer = new NetworkTrainer();

            //Add training sets; Training set needs example inputs which it will provide for the network, and the correct answer it will expect in return.
            trainer.trainingSets.Add(new TrainingSet(new double[2]
            {
                0, 1
            }, new double[1]
            {
                1
            }));

            trainer.trainingSets.Add(new TrainingSet(new double[2]
            {
                0, 0
            }, new double[1]
            {
                0
            }));

            trainer.trainingSets.Add(new TrainingSet(new double[2]
            {
                1, 0
            }, new double[1]
            {
                1
            }));

            trainer.trainingSets.Add(new TrainingSet(new double[2]
            {
                1, 1
            }, new double[1]
            {
                0
            }));

            //Train the network a few times using the trainer.
            NeuralNetwork trained = trainer.TrainNetwork(network, 10000, false);

            //Output results from the best network from training.
            Console.WriteLine("DONE");
            Console.WriteLine("");
            Console.WriteLine("Best network test: (" + trained.Layers.Count + " layers)");

            double o = 0;

            o = trained.GetOutput(new double[2] { 0, 1 })[0];
            Console.WriteLine("0, 1 - Expecting 1: " + o + " (" + Math.Round(o, 5) + ")");

            o = trained.GetOutput(new double[2] { 1, 0 })[0];
            Console.WriteLine("1, 0 - Expecting 1: " + o + " (" + Math.Round(o, 5) + ")");

            o = trained.GetOutput(new double[2] { 1, 1 })[0];
            Console.WriteLine("1, 1 - Expecting 0: " + o + " (" + Math.Round(o, 5) + ")");

            o = trained.GetOutput(new double[2] { 0, 0 })[0];
            Console.WriteLine("0, 0 - Expecting 0: " + o + " (" + Math.Round(o, 5) + ")");

            Console.WriteLine("Press RETURN to show network string.");

            Console.Read();
            Console.Read();

            Console.WriteLine("Network String");

            trained.Fitness = 5;

            string trainedS = trained.ToString();
            NeuralNetwork parsed = NeuralNetwork.Parse(trainedS);
            o = parsed.GetOutput(new double[2] { 0, 1 })[0];
            Console.WriteLine("0, 1 - Expecting 1: " + o + " (" + Math.Round(o, 5) + ")");

            o = parsed.GetOutput(new double[2] { 1, 0 })[0];
            Console.WriteLine("1, 0 - Expecting 1: " + o + " (" + Math.Round(o, 5) + ")");

            o = parsed.GetOutput(new double[2] { 1, 1 })[0];
            Console.WriteLine("1, 1 - Expecting 0: " + o + " (" + Math.Round(o, 5) + ")");

            o = parsed.GetOutput(new double[2] { 0, 0 })[0];
            Console.WriteLine("0, 0 - Expecting 0: " + o + " (" + Math.Round(o, 5) + ")");

            Console.WriteLine(trainedS);
        }
    }
}
