﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MachineLearning
{
    /// <summary>
    /// A simple trainer for a Neural Network.
    /// </summary>
    public class NetworkTrainer
    {
        /// <summary>
        /// Training sets to be used in the trainer.
        /// </summary>
        public List<TrainingSet> trainingSets = new List<TrainingSet>();
        
        /// <summary>
        /// Train a network, which eventually returns a new trained network.
        /// </summary>
        /// <param name="originalNetwork">Original network to base the trained networks off.</param>
        /// <param name="cycles">Amount of training cycles to perform. More cycles generally mean better accuracy.</param>
        /// <param name="outputToConsole">Output training data to the console. (much slower)</param>
        /// <returns>A trained network.</returns>
        public NeuralNetwork TrainNetwork(NeuralNetwork originalNetwork, int cycles, bool outputToConsole = false)
        {
            double inaccuracy = double.MaxValue;
            NeuralNetwork best = originalNetwork;

            int improvements = 0;
            
            Parallel.For(0, cycles, c=>
            {
                NeuralNetwork network = best.Clone();
                double currentInaccuracy = 0;

                network.Mutate(10);

                for (int i = 0; i < trainingSets.Count; i++)
                {
                    currentInaccuracy += trainingSets[i].GetNetworkInaccuracy(network, outputToConsole);
                }
                
                if (currentInaccuracy <= inaccuracy) //Better
                {
                    best = network;
                    inaccuracy = currentInaccuracy;
                    improvements++;
                }
            });

            return best;
        }
    }
}
