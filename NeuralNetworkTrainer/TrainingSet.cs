﻿using System;

namespace MachineLearning
{
    /// <summary>
    /// A trainingset which can measure a network's accuracy.
    /// </summary>
    public class TrainingSet
    {
        private double[] inputs, outputs;

        /// <summary>
        /// A trainingset which can measure a network's accuracy.
        /// </summary>
        /// <param name="inputs">Input values given to the network for this training set.</param>
        /// <param name="expectedOutputs">Outputs to be expected from a perfect network.</param>
        public TrainingSet(double[] inputs, double[] expectedOutputs)
        {
            this.inputs = inputs;
            outputs = expectedOutputs;
        }

        /// <summary>
        /// Calculates the inaccuracy of a network. A lower value means a better network, zero being the perfect network.
        /// </summary>
        /// <param name="network">The network to assess</param>
        /// <param name="outputToConsole">Output training data to console. (much slower)</param>
        /// <returns>inaccuracy of the network. Lower is better.</returns>
        public double GetNetworkInaccuracy(NeuralNetwork network, bool outputToConsole = false)
        {
            double[] networkOutputs = network.GetOutput(inputs);

            double inaccuracy = 0; //Lower is better!

            if(networkOutputs.Length != outputs.Length)
                throw new InvalidOperationException("Training outputs do not match network output neurons!");

            for (int i = 0; i < outputs.Length; i++)
            {
                double diff = Math.Abs(networkOutputs[i] - outputs[i]);

                if (outputToConsole)
                    Console.WriteLine("Output: " + networkOutputs[i] + " Expected: " + outputs[i] + ". Error: " + diff);

                inaccuracy += diff;
            }

            return inaccuracy;
        }
    }
}
